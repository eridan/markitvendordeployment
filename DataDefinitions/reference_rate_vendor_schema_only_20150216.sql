-- MySQL dump 10.13  Distrib 5.5.37, for Linux (x86_64)
--
-- Host: localhost    Database: reference_rate_vendor
-- ------------------------------------------------------
-- Server version	5.5.37-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `reference_rate_vendor`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `reference_rate_vendor` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `reference_rate_vendor`;

--
-- Table structure for table `EURIBOR`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EURIBOR` (
  `val_date` date NOT NULL,
  `1w` double DEFAULT NULL,
  `2w` double DEFAULT NULL,
  `3w` double DEFAULT NULL,
  `1m` double DEFAULT NULL,
  `2m` double DEFAULT NULL,
  `3m` double DEFAULT NULL,
  `4m` double DEFAULT NULL,
  `5m` double DEFAULT NULL,
  `6m` double DEFAULT NULL,
  `7m` double DEFAULT NULL,
  `8m` double DEFAULT NULL,
  `9m` double DEFAULT NULL,
  `10m` double DEFAULT NULL,
  `11m` double DEFAULT NULL,
  `12m` double DEFAULT NULL,
  PRIMARY KEY (`val_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `REFERENCE_RATE`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `REFERENCE_RATE` (
  `REF_RATE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `REF_RATE_NAME` varchar(255) DEFAULT NULL,
  `NAME_MARKIT` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`REF_RATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `REFERENCE_RATE_VALUE`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `REFERENCE_RATE_VALUE` (
  `REF_RATE_VALUE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `REF_RATE_ID` int(11) NOT NULL,
  `REF_RATE_NAME` varchar(255) DEFAULT NULL,
  `VAL_DATE` date NOT NULL,
  `VALUE` double DEFAULT NULL,
  PRIMARY KEY (`REF_RATE_VALUE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18672 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=1489;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'reference_rate_vendor'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-16 13:22:40
