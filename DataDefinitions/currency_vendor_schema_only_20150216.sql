-- MySQL dump 10.13  Distrib 5.5.37, for Linux (x86_64)
--
-- Host: localhost    Database: currency_vendor
-- ------------------------------------------------------
-- Server version	5.5.37-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `currency_vendor`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `currency_vendor` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `currency_vendor`;

--
-- Table structure for table `exchange_rate`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exchange_rate` (
  `source_currency_id` int(11) unsigned NOT NULL,
  `target_currency_id` int(11) unsigned NOT NULL,
  `obs_date` date NOT NULL,
  `rate` decimal(15,6) DEFAULT NULL,
  PRIMARY KEY (`target_currency_id`,`obs_date`,`source_currency_id`),
  KEY `FK_reference_rate_tbl_currency_currency_id_source` (`source_currency_id`),
  CONSTRAINT `FK_reference_rate_tbl_currency_currency_id_source` FOREIGN KEY (`source_currency_id`) REFERENCES `tbl_currency` (`currency_id`),
  CONSTRAINT `FK_reference_rate_tbl_currency_currency_id_target` FOREIGN KEY (`target_currency_id`) REFERENCES `tbl_currency` (`currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `linkage_history`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linkage_history` (
  `linkage_index_id` int(11) unsigned NOT NULL,
  `obs_date` date NOT NULL,
  `index_value` decimal(15,6) NOT NULL,
  PRIMARY KEY (`linkage_index_id`,`obs_date`),
  CONSTRAINT `FK_linkage_index_history_linkage_index_list` FOREIGN KEY (`linkage_index_id`) REFERENCES `linkage_list` (`linkage_index_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=62 COMMENT='The table includes the history of rates that used for bond l';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `linkage_list`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linkage_list` (
  `linkage_index_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name_english` varchar(200) NOT NULL,
  `country_symbol` varchar(255) DEFAULT NULL,
  `first_trading_date` date DEFAULT NULL,
  `last_trading_date` date DEFAULT NULL,
  `up_date` date DEFAULT NULL,
  `data_source` varchar(255) DEFAULT NULL,
  `rate_publish_rule` int(11) DEFAULT NULL,
  PRIMARY KEY (`linkage_index_id`),
  UNIQUE KEY `index_id_UNIQUE` (`linkage_index_id`),
  UNIQUE KEY `index_name_UNIQUE` (`name_english`)
) ENGINE=InnoDB AUTO_INCREMENT=9846 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=682 COMMENT='The table includes list of rates for bond linkage like Libor';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_currency`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_currency` (
  `currency_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `currency_code` varchar(255) NOT NULL,
  `name_english` text,
  PRIMARY KEY (`currency_id`),
  UNIQUE KEY `country_code` (`currency_code`)
) ENGINE=InnoDB AUTO_INCREMENT=419 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=65;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'currency_vendor'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-16 14:02:32
