now=$(date +""%Y%m%d"")
mysqldump --no-data --routines --triggers --create-options --skip-add-drop-table --databases markit_vendor > /data/markit_vendor_schema_only_$now.sql.gz
scp -r  /data/markit_vendor_schema_only_20150210.sql.gz  root@10.0.125.107:/root/DataImport/Markit/DataDefinitions

mysqldump --no-data --routines --triggers --create-options --skip-add-drop-table --databases data_vendor_log_schema > /data/data_vendor_log_schema_only_$now.sql.gz
scp -r  /data/data_vendor_log_schema_only_20150210.sql.gz  root@10.0.125.107:/root/DataImport/Markit/DataDefinitions

mysqldump --skip-triggers --no-create-info   markit_vendor --tables code_table_list> /data/code_table_list_20150210.sql
scp -r  /data/code_table_list_20150210.sql  root@10.0.125.107:/root/DataImport/Markit/DataDefinitions

mysqldump --skip-triggers --no-create-info   markit_vendor --tables code_tables_values_20150210> /data/code_tables_values._20150210/sql
scp -r  /data/code_tables_values_20150210.sql  root@10.0.125.107:/root/DataImport/Markit/DataDefinitions

mysqldump --skip-triggers --no-create-info   markit_vendor --tables control_exclusion_table_20150210> /data/control_exclusion_table.sql
scp -r  /data/control_exclusion_table_20150210.sql  root@10.0.125.107:/root/DataImport/Markit/DataDefinitions

mysqldump --no-data --routines --triggers --create-options --skip-add-drop-table --databases reference_rate_vendor > /data/reference_rate_vendor_schema_only_20150216.sql
scp -r   /data/reference_rate_vendor_schema_only_20150216.sql  root@10.0.125.107:/root/DataImport/Markit/DataDefinitions

mysqldump --skip-triggers --no-create-info   reference_rate_vendor  --tables EURIBOR REFERENCE_RATE REFERENCE_RATE_VALUE> /data/reference_rate_vendor_20150216.sql
scp -r  /data/reference_rate_vendor_20150216.sql  root@10.0.125.107:/root/DataImport/Markit/DataDefinitions

mysqldump --no-data --routines --triggers --create-options --skip-add-drop-table --databases currency_vendor > /data/currency_vendor_schema_only_$now.sql.gz
scp -r  /data/currency_vendor_schema_only_20150216.sql  root@10.0.125.107:/root/DataImport/Markit/DataDefinitions

mysqldump --skip-triggers --no-create-info   currency_vendor  --tables exchange_rate linkage_history  linkage_list tbl_currency > /data/currency_vendor_tbls_20150216.sql
 scp -r  /data/currency_vendor_tbls_20150216.sql  root@10.0.125.107:/root/DataImport/Markit/DataDefinitions
 
mysqldump --skip-triggers --no-create-info   global_market  --tables exchange_rate  > /data/exchange_rate_tbl_20150216.sql
scp -r  /data/exchange_rate_tbl_20150216.sql  root@10.0.125.107:/root/DataImport/Markit/DataDefinitions

mysqldump --skip-triggers --no-create-info   global_market  --tables tbl_currency  > /data/tbl_currency_tbl_20150216.sql
scp -r  /data/tbl_currency_tbl_20150216.sql  root@10.0.125.107:/root/DataImport/Markit/DataDefinitions

