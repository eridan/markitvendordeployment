mkdir /root/DataImport
mkdir /root/DataImport/Markit
mkdir /root/DataImport/Markit/DataDefinitions
mysql -N<markit_vendor_schema_only_20150210.sql.gz
mysql markit_vendor  -N<code_table_list_20150210.sql
mysql markit_vendor  -N<code_tables_values_20150210.sql
mysql markit_vendor  -N<control_exclusion_table_20150210.sql
mysql   -N<data_vendor_log_schema_only_20150210.sql.gz
mysql   -N<reference_rate_vendor_schema_only_20150216.sql
mysql  reference_rate_vendor  -N<reference_rate_vendor_20150216.sql
mysql   -N<currency_vendor_schema_only_20150216.sql
mysql currency_vendor_schema  -N<currency_vendor_tbls_20150216.sql
mysql global_market  -N<exchange_rate_tbl_20150216.sql
mysql global_market  -N<tbl_currency_tbl_20150216.sql